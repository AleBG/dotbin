#!/usr/bin/env bats

load "${HOME}/.bin/calc"

@test "calc returns 4 for << 2 + 2 >>" {
  run calc 2 + 2
  [ "$status" -eq 0 ]
  [ "$output" = "4" ]
}

@test "calc returns 0 for << 2 - 2 >>" {
  run calc 2 - 2
  [ "$status" -eq 0 ]
  [ "$output" = "0" ]
}

@test "calc returns 4 for << 2 '*' 2 >>" {
  run calc 2 '*' 2
  [ "$status" -eq 0 ]
  [ "$output" = "4" ]
}

@test "calc returns 1.50 for << 3 / 2 >>" {
  run calc 3 / 2
  [ "$status" -eq 0 ]
  [ "$output" = "1.50" ]
}

@test "calc returns 1 for << 3 % 2 >>" {
  run calc 3 % 2
  [ "$status" -eq 0 ]
  [ "$output" = "1" ]
}

@test "calc returns 8 for << 2 ^ 3 >>" {
  run calc 2 ^ 3
  [ "$status" -eq 0 ]
  [ "$output" = "8" ]
}

@test "calc returns 0 for << 4 - 2 '*' 2 >>" {
  run calc 4 - 2 '*' 2
  [ "$status" -eq 0 ]
  [ "$output" = "0" ]
}
