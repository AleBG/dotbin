#!/usr/bin/env bats

load "${HOME}/.bin/doi2bib"

@test "is_doi returns 0 for valid DOI" {
  run is_doi "10.1000/xyz123"
  [ "$status" -eq 0 ]
}

@test "is_doi returns 1 for invalid DOI" {
  run is_doi "invalid_doi"
  [ "$status" -eq 1 ]
}