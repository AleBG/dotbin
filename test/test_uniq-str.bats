#!/usr/bin/env bats

load "${HOME}/.bin/uniq-str"

@test "uniq-str returns different hash even when executed in parallel" {
    # Run the script in parallel
    result1=$(bash uniq-str &)
    result2=$(bash uniq-str &)

    # Wait for all background jobs to finish
    wait

    # Assert that the results are not empty
    [ -n "$result1" ]
    [ -n "$result2" ]

    # Assert that the results are different
    [ "$result1" != "$result2" ]
}