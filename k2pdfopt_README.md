Use this command to create a TOC in an existing PDF:

./k2pdfopt -mode copy -n -toclist my_toc.txt srcfile.pdf -o outfile.pdf

The TOC file has to be a plain text .txt file, with the following structure:

```txt
1 Introduction
10 Chapter 1
+10 Chapter 1, Part A
+25 Chapter 1, Part B
++25 Chapter 1, Part B, Subsection 1
++27 Chapter 1, Part B, Subsection 2
+30 Chapter 1, Part C
50 Chapter 2
70 Chapter 3
```

Example: Woodward's Making Things Happen:

```txt
1 Front Matter
8 Contents
12 1. Introduction and Preview
34 2. Causation and Manipulation
103 3. Interventions, Agency, and Counterfactuals
161 4. Causal Explanation: Background and Criticism
196 5. A Counterfactual Theory of Causal Explanation
248 6. Invariance
324 7. Causal Interpretation in Structural Models
359 8. The Causal Mechanical and Unificationist Models of Explanation
383 Afterword
385 Notes
+385 Ch. 1
+385 Ch. 2
+393 Ch. 3
+396 Ch. 4
+399 Ch. 5
+401 Ch. 6
+404 Ch. 7
+406 Ch. 8 & Afterword
407 References
416 Index
```


See more at:

https://www.willus.com/k2pdfopt/help/options.shtml
