#
# Remove these 4 lines and make it executable to use it again
#

#!/usr/bin/env bash

# Converts a Markdown files to PDF files using pandoc
    # Requires pandoc (v > 2.0)
    # Removes the initial "." if the original .md starts with "."

# USE: panmd-to-pdf <FILE1> <FILE2> ...
    # Uses a basic YAML file at ~/.basic-format.yaml if it exists
# USE 2: panmd-to-pdf -f=<FORMAT.yaml> <FILE1> <FILE2> ...
    # Uses a different YAML file

# For processing a large amount of files in a slow computer,
# better set this at a low value ( e.g. 3 )
max_jobs=3


if [ "$1" = "-f" ]; then

    format=$2
    if [ -f "$format" ]; then

        for file in "${@:3}" ; do

            while [ $(jobs -r | wc -l | tr -d " ") -ge $max_jobs ]; do
                wait # ; sleep 1
            done

            if [ ${file:0:1} = "." ]; then
                nodot=${file:1}
                target=${nodot%.md}.pdf
                pandoc -d "$format" -o "$target" "$file" &
            else
                target=${file%.md}.pdf
                pandoc -d "$format" -o "$target" "$file" &
            fi
        done

    else
        echo "The file $format does not exist; aborting."
    fi

else
    format=~/basic-format.yaml
    if [ -f "$format" ]; then

        for file in "$@" ; do

            while [ $(jobs -r | wc -l | tr -d " ") -ge $max_jobs ]; do
                wait # ; sleep 1
            done

            if [ ${file:0:1} = "." ]; then
                nodot=${file:1}
                target=${nodot%.md}.pdf
                pandoc -d "$format" -o "$target" "$file" &
            else
                target=${file%.md}.pdf
                pandoc -d "$format" -o "$target" "$file" &
            fi
        done

    else
        for file in "$@" ; do

            while [ $(jobs -r | wc -l | tr -d " ") -ge $max_jobs ]; do
                wait # ; sleep 1
            done

            if [ ${file:0:1} = "." ]; then
                nodot=${file:1}
                target=${nodot%.md}.pdf
                pandoc -o "$target" "$file" &
            else
                target=${file%.md}.pdf
                pandoc -o "$target" "$file" &
            fi
        done
    fi
fi

