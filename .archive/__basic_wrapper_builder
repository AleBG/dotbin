#!/usr/bin/env bash


function usage() {
cat << EOF
__basic_wrapper_builder [CONTAINER] [PROGRAM1] [PROGRAM2] ...
    Builds basic wrappers to use programs inside containers. Will output one wrapper (bash script) for each program in the CWD, each wrapper called like the program.

    EXAMPLE: __basic_wrapper_builder redactor nvim pandoc
    

EOF
}

function builder() {
    local container="${1}"
    local program="${2}"
    cat << EOF
#!/usr/bin/env bash

# DEPENDENCIES
    # The ${container} container and corresponding wrapper 
    # The container needs ${program} installed

${container} ${program} "\${@:1}"

EOF
}


case "$1" in
    -h | --help | "")
    usage
    ;;

    *)
    for program in "${@:2}" ; do
        builder "${1}" "${program}" > "${program}"
        chmod +x "${program}"
    done
    ;;
esac
