############
# SETUP
############

# Chose base image from dockerhub
FROM debian:unstable-slim

# Change the default shell for the RUN command
    # Note that the path is system-specific!
SHELL ["/bin/bash", "-c"]

# You can define variables to use later in the dockerfile
ARG user=user1
ARG home=/home/$user
ARG passw=debian

############
# USERS
############

# Create a user with a home directory and bash as default shell
RUN useradd --create-home --shell=/bin/bash $user \
    # Set the password of $user to be $passw
    && echo $user:$passw | chpasswd \
    # Add to sudoers, but note that sudo needs to be installed
    && adduser $user sudo
    # But you need to apt install sudo, otherwise just login as root

# WORKDIR and USER set environment for most of the build commands after them
    # Including RUN, CMD, ENTRYPOINT for both
    # Plus COPY and ADD for WORKDIR
    # Both define the default --user and --workdir after docker run
WORKDIR $home
USER $user

############
# FILES AND COMMANDS
############

# Create a hard-coded launch script
#RUN printf '#!/usr/bin/env bash\n\
#\n\
#echo "hello world!"\n\
#echo "hello!" > testfile'\
#> ~/test.sh

# It's usually better to bring the scripts from the outside
#   Note: docker recommends to NOT use ADD, use COPY
#   The owner defaults to root, so use the --chown option
COPY --chown=$user 0debtestlaunch.sh test.sh
# Don't forget to give it execution permission if it's an executable
RUN chmod +x test.sh

# Defines the entrypoint script to execute
#   this happens BEFORE CMD
#   this is overriden by the --entrypoint option
#   If this is uncommented, the containers created will just execute test.sh
#   and that's it
#   To use an interactive shell from within, we will need to run them with
#   --entrypoint=/bin/bash to override the line below
#ENTRYPOINT ["/bin/bash", "./test.sh"]

# Defines the default command to be (docker) run
#   this is overriden by any argument passed to docker run
#   and may be overriden by ENTRYPOINT, it's confusing, check the docks
#   Note: only the last CMD in the dockerfile will be kept
#CMD echo "This was build as CMD."
