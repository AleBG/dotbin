
Goal:
use CLI `nbconvert` to convert `.ipynb` files to pretty PDFs;
this is used by jupyter's "export to PDF" feature;
BUT! nbconvert uses pdflatex, xelatex, pandoc, inkscape, bibtex, all of them in a different container;
so I need to somehow let nbconvert know that these dependencies are in that other container (debitex).


Attempt 1:
use docker sibling containers (run debipy with -v /var/run/docker.sock:/var/run/docker.sock)
and use bash wrappers that call "docker run ... debitex xelatex [ARGs]" (also with pandoc, etc.), with those wrappers at PATH (`/usr/bin`) so nbconvert calls what it needs with the wrappers.

Seems to work up to a certain point, not sure why I'm having this error:

```
jupyter nbconvert --debug --to pdf a.\ Nooby\ habits\ to\ ditch.ipynb
[NbConvertApp] Searching ['/shared', '/home/alepy/.jupyter', '/home/alepy/.local/etc/jupyter', '/usr/local/etc/jupyter', '/etc/jupyter'] for config files
[NbConvertApp] Looking for jupyter_config in /etc/jupyter
[NbConvertApp] Looking for jupyter_config in /usr/local/etc/jupyter
[NbConvertApp] Looking for jupyter_config in /home/alepy/.local/etc/jupyter
[NbConvertApp] Looking for jupyter_config in /home/alepy/.jupyter
[NbConvertApp] Looking for jupyter_config in /shared
[NbConvertApp] Looking for jupyter_nbconvert_config in /etc/jupyter
[NbConvertApp] Looking for jupyter_nbconvert_config in /usr/local/etc/jupyter
[NbConvertApp] Looking for jupyter_nbconvert_config in /home/alepy/.local/etc/jupyter
[NbConvertApp] Looking for jupyter_nbconvert_config in /home/alepy/.jupyter
[NbConvertApp] Looking for jupyter_nbconvert_config in /shared
[NbConvertApp] Converting notebook a. Nooby habits to ditch.ipynb to pdf
[NbConvertApp] Notebook name is 'a. Nooby habits to ditch'
[NbConvertApp] Template paths:
	/home/alepy/.local/share/jupyter/nbconvert/templates/latex
	/home/alepy/.local/share/jupyter/nbconvert/templates/base
	/home/alepy/.local/share/jupyter
	/home/alepy/.local/share/jupyter/nbconvert/templates
	/home/alepy/.local/share/jupyter/nbconvert/templates/compatibility
	/usr/local/share/jupyter
	/usr/local/share/jupyter/nbconvert/templates
	/usr/local/share/jupyter/nbconvert/templates/compatibility
	/usr/share/jupyter
	/usr/share/jupyter/nbconvert/templates
	/usr/share/jupyter/nbconvert/templates/compatibility
	/home/alepy/.local/share/jupyter/nbconvert/templates
[NbConvertApp] Applying preprocessor: TagRemovePreprocessor
[NbConvertApp] Applying preprocessor: RegexRemovePreprocessor
[NbConvertApp] Applying preprocessor: coalesce_streams
[NbConvertApp] Applying preprocessor: SVG2PDFPreprocessor
[NbConvertApp] Applying preprocessor: LatexPreprocessor
[NbConvertApp] Applying preprocessor: HighlightMagicsPreprocessor
[NbConvertApp] Applying preprocessor: ExtractOutputPreprocessor
[NbConvertApp] Attempting to load template index.tex.j2
[NbConvertApp]     template_paths: /home/alepy/.local/share/jupyter/nbconvert/templates/latex:/home/alepy/.local/share/jupyter/nbconvert/templates/base:/home/alepy/.local/share/jupyter:/home/alepy/.local/share/jupyter/nbconvert/templates:/home/alepy/.local/share/jupyter/nbconvert/templates/compatibility:/usr/local/share/jupyter:/usr/local/share/jupyter/nbconvert/templates:/usr/local/share/jupyter/nbconvert/templates/compatibility:/usr/share/jupyter:/usr/share/jupyter/nbconvert/templates:/usr/share/jupyter/nbconvert/templates/compatibility:/home/alepy/.local/share/jupyter/nbconvert/templates
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
JSON parse error: Error in $: not enough input
[NbConvertApp] Writing 38933 bytes to notebook.tex
[NbConvertApp] Building PDF
[NbConvertApp] Running xelatex 3 times: ['xelatex', 'notebook.tex', '-quiet']
[NbConvertApp] Running bibtex 1 time: ['bibtex', 'notebook']
[NbConvertApp] Running xelatex 3 times: ['xelatex', 'notebook.tex', '-quiet']
Traceback (most recent call last):
  File "/home/alepy/.local/bin/jupyter-nbconvert", line 8, in <module>
    sys.exit(main())
  File "/home/alepy/.local/lib/python3.10/site-packages/jupyter_core/application.py", line 269, in launch_instance
    return super().launch_instance(argv=argv, **kwargs)
  File "/home/alepy/.local/lib/python3.10/site-packages/traitlets/config/application.py", line 846, in launch_instance
    app.start()
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/nbconvertapp.py", line 414, in start
    self.convert_notebooks()
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/nbconvertapp.py", line 588, in convert_notebooks
    self.convert_single_notebook(notebook_filename)
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/nbconvertapp.py", line 551, in convert_single_notebook
    output, resources = self.export_single_notebook(
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/nbconvertapp.py", line 479, in export_single_notebook
    output, resources = self.exporter.from_filename(
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/exporters/exporter.py", line 189, in from_filename
    return self.from_file(f, resources=resources, **kw)
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/exporters/exporter.py", line 206, in from_file
    return self.from_notebook_node(
  File "/home/alepy/.local/lib/python3.10/site-packages/nbconvert/exporters/pdf.py", line 200, in from_notebook_node
    raise LatexFailed("\n".join(self._captured_output))
nbconvert.exporters.pdf.LatexFailed: PDF creating failed, captured latex output:
```

Attempts to fix:
  + Not sure why `JSON parse error: Error in $: not enough input` happens, internet is not helping
  + [not yet tried!] instead of using "docker run.." for each time we need debitex, maybe we can `docker start` an instance of debitex and leave it running,
then make `nbconvert` use that container (`docker exec`), and stop it only *after* nbconvert finishes.
Reasoning: maybe latex is not finding its cache or something? as it has to be called 3+ times but each time it's in a new container


Working fix:

    - Interface jupyter and latex containers using a docker volume
```bash
# @host:
docker volume create --name docker-tmp
docker run --rm --volume="/var/run/docker.sock:/var/run/docker.sock" -ti        --name="debiPyContainer20220423194924" --volume="docker-tmp:/docker-tmp"        --volume="/home/ale/test:/shared" -p 8888:8888 luisalebg/debipydckr:test bash

# @clientSibling
sudo docker run -ti --user=root --volume="docker-tmp:/shared" --workdir=/shared luisalebg/debitex manual

# @LaTeXserverSibling
xelatex # ... it works!
```
So, change the bash xelatex wrapper in client (debipy) to include the docker volume

