# debipy January 2022

Debian unstable container for python 3.9, jupyter and data analysis.
All jupyter kernels are virtualenvs:

- `data-science` contains:
	+ `numpy`
	+ `pandas`
	+ `scipy`
	+ `matplotlib`

- `dbSQLmgmt` contains:
	+ `psycopg2`: postgreSQL databases management.
Had to `apt install python3-dev libpg-dev` in the system for it to work
	+ `SQAlchemy`
	+ `ERAlchemy`: to produce ER diagrams from databases.
Had to `apt install libgraphviz-dev` in the system for it to work

- `networkanalysis` contains:
	+ `networkx`
	+ `igraph`
	+ `pyvis`
