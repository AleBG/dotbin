#!/usr/bin/env bash

# Entrypoint for a jupyterlab server container,
#   meant to be used with:
#docker run --rm -ti -p 8888:8888 -v ${PWD}:/shared image
#docker run --rm -ti -e JOVYAN_PORT=1234 -p 1234:1234 -v ${PWD}:/shared image
#docker run --rm image [ -h | --help | help ]
#docker run --rm image [ -v | --versions | versions ]

# Save this as somewhere at PATH as jovyanlaunch and give it chmod +x

# Help messages
case "$1" in
    "-h" | "--help" | "help")
        help.msg
        exit 0 ;;
    "-v" | "--versions" | "versions")
        help_versions.msg
        exit 0 ;;
esac

# Change into the shared directory with the host
cd /shared

# Start a local jupyter lab server exposing container's port 8888
# defined as an ENV variable at build
# to override, run the container with -e JOVYAN_PORT='1234'
jupyter lab --ip='0.0.0.0' --port=${JOVYAN_PORT} --no-browser --allow-root
