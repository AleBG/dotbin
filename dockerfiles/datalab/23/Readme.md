# datalab November 2023

Debian unstable container for python 3.11, R 4.2.2, jupyter and data science in general.
All jupyter kernels are virtualenvs:

- `datalab` contains:
	+ `numpy`
	+ `pandas`
	+ `scipy`
	+ `matplotlib`
	+ `networkx`
	+ `igraph`
	+ `pyvis`


All R datascience packages are installed in the /R directory.
This means that you need a local ~/.Rprofile file with the following line:
```R
.libPaths(c("/R", .libPaths()))
```
in order to be able to call the R packages without needing to add `loc.lib="/R"` to every call of `library()`.
With this, IRKernel should be able to find the R packages as well.