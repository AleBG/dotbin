Images to use the [`sdaps` program](https://sdaps.org/) for survey design and processing.
It allows for `sdaps gui` and `texstudio` sessions from within the container if run as recommended below.

Recommended use:
```bash
docker run --rm -ti -v "\${PWD}:/shared" \\
           --env=DISPLAY=\${DISPLAY} \\
           --volume='/tmp/.X11-unix/:/tmp/.X11-unix/' \\
           ${DOCKER_IMAGE_TAG} [valid sdaps ARGUMENTS | texstudio]
docker run --rm ${DOCKER_IMAGE_TAG} [ -h | --help | help ]
docker run --rm ${DOCKER_IMAGE_TAG} [ -he | --help-example | help-example ]
```

## `latest`

- Mirrors the next tag, which should be the latest functioning one

## `22jan-bionic`

- Built from `ubuntu:bionic` (Ubuntu 18) as it was on January 2022
- Has a `/shared` directory with full access to all users, meant to be used as a volume (to have shared persistent storage) when creating a container
- The default user "omr" is a sudoer with password "sdaps"

## `jan22OldFunctional`

- Built from `ubuntu:bionic` manually with commits, without a Dockerfile.
It's functional but ugly.

